CSS - Librairie d'Animation 
======================
<dl><dt>Auteur</dt>
  <dd>Félix "DiglidiDudeNG" Dion-Robidoux</dd>

  <dt>Langage</dt>
  <dd>CSS3</dd>
  
  <dt>Version</dt>
  <dd>1.3 (8 Juin 2014)</dd>
</dl>

### Description
Cette librairie d'animations en CSS3 offre 5 différentes animations personnalisées
et attrayantes qui sauront impressionner vos visiteurs !

### Contenu
1. L'éternuement `.animlib_sneeze`
  * Fait atchoumer l'élément et le rend invisible pendant l'expulsion du pus! (non-inclus)
2. PopUp style Deus Ex: Human Revolution `.animlib_dehr_popup`
  * Fait apparaître l'élément dans le style d'interface du jeu Deus Ex: Human Revolution.
3. Journal style Deux Ex: Human Revolution `.animlib_dehr_journal`
  * Fait apparaître l'élément comme dans DE:HR lorsque le joueur prend un journal.
4. Bouton style Windows 8 `.animlib_win8btn`
  * Ajoute une animation à votre bouton au style de Windows 8.
5. Bordure de côté style Google `.animlib_bordureGoogle`
  * Comme sur Google+, une bordure sur le côté sort vers le côté intérieur de l'élément.

### Installation
1. Insérez le fichier `animation_lib_dionrobidouxfelix.css` dans l'emplacement de 
	vos librairies css de votre projet (ex: `racineDuProjet\css\lib\`)
2. Dans vos fichiers utilisant du CSS, tel que les .html, ajoutez dans la balise `<head>` la référence de style de la librairie. 
	(ex: `<script src="./css/lib/animation_lib_dionrobidouxfelix.css"></script>`)
	**ATTENTION:**  Mettez cette référence de script avant tout autre référence de script
	qui fait mention d'une des 5 animations dans son code, sinon ça risque de
	ne pas bien fonctionner !
3. Lorsque vous souhaitez qu'un de vos éléments utilises l'une de mes 5 animations, 
	insérez-y la classe correspondante. Référez-vous à l'extrème-droite du nom de l'animation
	dans la section « Contenu » de ce fichier texte pour le nom des classes.

### Modification
Vous souhaitez changer l'animation pour l'adapter à votre site davantage? Aucun problème!

#### Comment 
Pour chaque élément(s homogènes) ayant besoin d'une des animations :

1. Ajoutez à votre fichier .css un sélecteur de votre élément animé.
2. Faites en sorte à ce que `animation_lib_dionrobidouxfelix.css` soit une ligne avant le script contenant
	la référence à l'une des classes de ma librairie, sinon vos modifications hors du fichier ne marcheront pas.
3. Dans le fichier .css de votre site, mettez un sélecteur vers l'élément contenant la classe de l'animation qu'il utilise, OU sélectionnez la classe directement si c'est pour la modifier globalement.
4. Ajoutez les propriétés dont vous souhaitez changer, en n'oubliant pas les préfixes de navigateurs (`-webkit-*`, `-moz-*`, `-o-*` et  `-ms-*`).

#### Propriétés modifiables

##### TOUTES LES ANIMATIONS EN KEYFRAMES
1. Durée totale
  * Ajoutez `animation-duration: *;` à votre sélecteur, où * est une valeur de temps de votre choix. (ex: `1s`, `500ms`, etc.)
2. Fonction de transition
  * Ajoutez `animation-timing-function: *;` à votre sélecteur, où * peut être un de ces termes :
    * `linear` : Garde la même vitesse tout le long.
    * `ease` : Vite au début, puis ralenti de plus en plus jusqu'à la fin.
    * `ease-in` : Lent au début, puis prend de la vitesse de plus en plus jusqu'à la fin.
    * `ease-out` : Vite au début, puis ralenti de plus en plus jusqu'à la fin.
    * `ease-in-out` : Très similaire à « ease » tout court, mais a une courbe symétrique à la place.
    * `cubic-bezier(*,*,*,*)` : Courbe personnalisée. Utilise 4 points entre 0 et 1.
      * `step-start` : Image-par-image, change d'image au début d'une fraction de l'animation.
      * `step-end` : Image-par-image, change d'image à la fin d'une fraction de l'animation.
      * `step(*[, end|start])` » : Image-par-image où * est un nombre au choix.
3. Délai de l'animation
  * Ajoutez `animation-delay: *;` à votre sélecteur, où * est une valeur de temps de votre choix. (ex: `1s`, `500ms`, etc.)
4. Nombre d'itérations
  * Ajoutez `animation-iteration-count: *;` à votre sélecteur, où * est un nombre d'itérations. Peut aussi être `infinite` pour la répéter sans fin.
5. Direction
  * Ajoutez `animation-direction: *;` à votre sélecteur, où * est soit `normal`, `reverse`, `alternate` ou `alternate-reverse`, mais peut aussi en utiliser plusieurs, séparés par un virgule chaque.
6. Mode de remplissage
  * Ajoutez `animation-fill-mode: *;` à votre sélecteur, où * est soit `none`, `forwards`, `backwards` ou `both`.

#### Crédits
Merci à :
 - Jean-Philippe Lambert, mon prof d'animation qui m'a aidé et a été un prof encourageant!
   (non je dis pas ça juste pour avoir une meilleur note! ^^)
 - MadameMochete, pour m'avoir supporté.
